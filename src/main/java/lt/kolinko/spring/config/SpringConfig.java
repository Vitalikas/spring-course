package lt.kolinko.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = "lt.kolinko.spring")
@PropertySource("classpath:properties/app.properties")
public class SpringConfig {

}
