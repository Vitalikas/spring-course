package lt.kolinko.spring.repositories;

import lombok.Getter;

@Getter
public enum Genre {
    POP,
    TECHNO,
    RAVE
}
